import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * EX07
 * Created by Karl on 3.03.2016.
 */
public class EX07 {

    /**
     * List of Friend objects.
     */
    private static List<Friend> friends = new ArrayList<>();

    /**
     * Main thang.
     * @param args commandline arguments
     */
    public static void main(String[] args) {

        String[] list = new String[4];

        friends = readFriendsFromFile("src/example.txt");
        for (Friend friend : friends) {
            System.out.println(friend.getFullName());
        }
        for (Friend friend : friends) {
            System.out.println(friend.getLastName());
        }
        for (Friend friend : friends) {
            System.out.println(friend.getNames());
        }
        System.out.println(findFriendByLastName("Ain").getFullName()); // Ain Ain
        System.out.println(findFriendByLastName("Kask").getFullName()); // Ain Ain
        //System.out.println(findFriendByLastName("Punn").getFullName()); // Baul Punn
        //System.out.println(findFriendByLastName("Rukis").getFullName()); // Kaera Jaan Rukis
        //System.out.println(findFriendByLastName("Tamm").getNames()); //[Drop, Table, User]
        //System.out.println(findFriendByLastName("Kaera")); //null
    }

    /**
     * Reads (a small) file and returns list of lines (strings).
     * @param filename Filename to be read.
     * @return List of lines.
     */
    public static List<Friend> readFriendsFromFile(String filename) {
        Path path = Paths.get(filename);
        EX07.friends.clear();
        List<String> lines = null;
        try {
            lines = Files.readAllLines(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (lines != null) {
            for (String line : lines) {
                    String[] names = line.split(" +");
                    friends.add(new Friend(names));
            }
        }
        return friends;
    }

    /**
     * Finds a Friend object according to last name of the object.
     * @param lastName Last name of Friend to look for.
     * @return Friend object, null if such Friend doesn't exist.
     */
    public static Friend findFriendByLastName(String lastName) {
        for (Friend friend : friends) {
            if (friend.getLastName().equals(lastName)) {
                return friend;
            }
        }
        return null;
    }
}
