import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.Collections;
        import java.util.List;

        import static org.junit.Assert.*;
/**
 * Created by Ago on 2016-03-02.
 */
public class EX07TestBonus {

    @Before
    public void setupList() {
        // let's try to reset the list
        try {
            Object mainObject = new EX07();
            Class<?> clazz = mainObject.getClass();
            Field listField = clazz.getField("friends");
            listField.set(null, new ArrayList<>());
        } catch (NoSuchFieldException e) {
            // pass - no friends
            //e.printStackTrace();
        } catch (SecurityException e) {
            // pass - not public
            //e.printStackTrace();
        } catch (IllegalAccessException e) {
            //e.printStackTrace();
        }
    }
    @Test(timeout = 1000)
    public void testFindFriendByLastNameSameLastNames() {

        EX07.readFriendsFromFile("matchinglastnames");

        TestFriend expected = new TestFriend("Baggins", Arrays.asList("Bilbo"));

        EX07Test.compareFriends(expected, EX07.findFriendByLastName("Baggins"));
    }

    private void checkFriend(Friend friend, String lastname, List<String> names) {
        assertEquals(lastname, friend.getLastName());
        assertEquals(names, friend.getNames());
    }

    @Test(timeout = 1000)
    public void testFindFriendByLastNameSameLastNamesHarder() {

        EX07.readFriendsFromFile("sorttest");
/*
Kuuse Puu
Kase Puu
Mis Teed
Vee Tee
Auto Tee
Raua Puur
Mis Teed
Raha Puur
Raua Puur
 */
        checkFriend(EX07.findFriendByLastName("Puu"), "Puu", Arrays.asList("Kase"));
        checkFriend(EX07.findFriendByLastName("Tee"), "Tee", Arrays.asList("Auto"));
        checkFriend(EX07.findFriendByLastName("Teed"), "Teed", Arrays.asList("Mis"));
        checkFriend(EX07.findFriendByLastName("Puur"), "Puur", Arrays.asList("Raha"));

    }
    @Test(timeout = 1000)
    public void testFindFriendByLastNameSameLastNamesSortAndSpaces() {

        EX07.readFriendsFromFile("sorttest");
/*
Kase Puu
    Mis Sa Teed
                    Vee               Tee
Auto Tee
Kas    Puu
Raua Puur
Mis  Teed
  Raha Puur
 Raua Puur
   Rah Puur
 */
        //checkFriend(EX07.findFriendByLastName("Puu"), "Puu", Arrays.asList("Kas"));
        checkFriend(EX07.findFriendByLastName("Tee"), "Tee", Arrays.asList("Auto"));
        checkFriend(EX07.findFriendByLastName("Teed"), "Teed", Arrays.asList("Mis"));
        checkFriend(EX07.findFriendByLastName("Puur"), "Puur", Arrays.asList("Raha"));

    }

    @Test(timeout = 1000)
    public void testFindFriendByLastNameSameLastNamesSortExtreme() {

        EX07.readFriendsFromFile("extremesort");
/*
Kase Puu
Kas Puu <- Kas < Kase
Rah a Tünn
Rah Tünn <- Rah < Raha, Rah a
Raha Tünn
Kask Puu
Part Prääks
par Prääks <- par < Pardid
Pardid Prääks
Kaunis Nimi On See Jah Ly
Kaunis Nimi On See Ja Ly <- ... Ja < ... Jah
Kaunis Nimi On See Jah Ly
 */
        /*
        System.out.println("tere tere".compareTo("terea tere"));
        List<String> test = Arrays.asList("rah tünn", "rah a tünn", "a");
        Collections.sort(test);
        System.out.println(test);
        */
        checkFriend(EX07.findFriendByLastName("Puu"), "Puu", Arrays.asList("Kas"));
        checkFriend(EX07.findFriendByLastName("Tünn"), "Tünn", Arrays.asList("Rah"));
        checkFriend(EX07.findFriendByLastName("Prääks"), "Prääks", Arrays.asList("par"));
        checkFriend(EX07.findFriendByLastName("Ly"), "Ly", Arrays.asList("Kaunis", "Nimi", "On", "See", "Ja"));

    }

    @Test(timeout = 1000)
    public void testReadFriendsFromFileWrongSpace() {

        List<Friend> actualList = EX07.readFriendsFromFile("wrongspace");
        List<TestFriend> expectedList = new ArrayList<>();
        TestFriend expected = new TestFriend("tulemast", Arrays.asList("tere"));
        expectedList.add(expected);


        EX07Test.compareLists(expectedList, actualList);
    }
    private void addToList(List<TestFriend> friends, String lastname, List<String> names) {
        TestFriend f = new TestFriend(lastname, names);
        friends.add(f);
    }
    @Test(timeout = 1000)
    public void testReadFriendsFromFileVeryWrongSpace() {

        List<Friend> actualList = EX07.readFriendsFromFile("verywrongspace");
        List<TestFriend> expectedList = new ArrayList<>();
        addToList(expectedList, "kaal", Arrays.asList("mati"));
        addToList(expectedList, "meeter", Arrays.asList("kurjam", "lurjam"));
        addToList(expectedList, "tühik", Arrays.asList("lõpus", "ka"));


        EX07Test.compareLists(expectedList, actualList);
    }
}
