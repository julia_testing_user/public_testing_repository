import java.util.List;

/**
 * Created by Ago on 2016-03-02.
 */
public class TestFriend {
    private String lastName;
    private List<String> names;

    public TestFriend(String lastName, List<String> names) {
        this.lastName = lastName;
        this.names = names;
    }
    public String getLastName() {
        return lastName;
    }
    public String getFullName() {
        return String.join(" ", names) + " " + lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setNames(List<String> names) {
        this.names = names;
    }

    public List<String> getNames() {
        return names;
    }

}
