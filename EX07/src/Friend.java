import java.util.Arrays;
import java.util.List;

/**
 * EX07 class.
 */
public class Friend {
    /**
     * Full name of a friend.
     */
    private String fullName;

    /**
     * List of first names of a friend.
     */
    private List<String> names;

    /**
     * Last name of a friend.
     */
    private String lastName;

    /**
     * String of first names of friend.
     */
    private String firstNames = "";

    /**
     * Creates a friend with names.
     * @param fullNameString - the corrected name line from text.
     */
    public Friend(String fullNameString) {
        this.fullName = fullNameString;
        List<String> listOfNames = Arrays.asList(fullName.split(" "));
        this.lastName = listOfNames.get(listOfNames.size() - 1);
        this.names = listOfNames.subList(0, listOfNames.size() - 1);
        for (String name : names) {
            this.firstNames += name + " ";
        }
    }

    /**
     * Gets the last name of friend object.
     * @return - Last name of friend.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Gets the full name of friend object.
     * @return - Full name of friend.
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Gets the list of names of friend object.
     * @return - List of names of friend.
     */
    public List<String> getNames() {
        return names;
    }

    /**
     * Gets the first names of friend.
     * @return - String of first names.
     */
    public String getFirstNames() {
        return firstNames;
    }
}
