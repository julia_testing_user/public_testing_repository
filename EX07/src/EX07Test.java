import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.Assert.*;

public class EX07Test {


    @Before
    public void setupList() {
        // let's try to reset the list
        try {
            Object mainObject = new EX07();
            Class<?> clazz = mainObject.getClass();
            Field listField = clazz.getField("friends");
            listField.set(null, new ArrayList<>());
        } catch (NoSuchFieldException e) {
            // pass - no friends
            //e.printStackTrace();
        } catch (SecurityException e) {
            // pass - not public
            //e.printStackTrace();
        } catch (IllegalAccessException e) {
            //e.printStackTrace();
        }
    }
    @Test
    public void testReadFriendsFromFileBasic() {

        TestFriend f1 = new TestFriend("Geraghty", new ArrayList<>(Arrays.asList("Dalene")));
        TestFriend f2 = new TestFriend("Mcfadin", new ArrayList<>(Arrays.asList("Danuta")));
        TestFriend f3 = new TestFriend("Gumbs", new ArrayList<>(Arrays.asList("Lesli")));
        TestFriend f4 = new TestFriend("Longstreet", new ArrayList<>(Arrays.asList("Chet")));
        TestFriend f5 = new TestFriend("Newbern", new ArrayList<>(Arrays.asList("Darin")));

        List<TestFriend> expectedList = new ArrayList<>();
        expectedList.add(f1);
        expectedList.add(f2);
        expectedList.add(f3);
        expectedList.add(f4);
        expectedList.add(f5);

        List<Friend> actualList = EX07.readFriendsFromFile("basicfriends");

        compareLists(expectedList, actualList);
    }

    @Test
    public void testReadFriendsFromFileEmptyFile() {
        List<TestFriend> expected = new ArrayList<>();
        compareLists(expected, EX07.readFriendsFromFile("empty"));
    }

    @Test
    public void testReadFriendsFromFileOneFriend() {
        TestFriend f = new TestFriend("Lanzoni", Arrays.asList("Fabio"));
        List<TestFriend> expected = new ArrayList<>(Arrays.asList(f));
        compareLists(expected, EX07.readFriendsFromFile("onefriend"));
    }

    @Test
    public void testReadFriendsFromFileLongList() {
        List<TestFriend> expected = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            TestFriend f = new TestFriend(i + "Lastname", Arrays.asList("Name" + i));
            expected.add(f);
        }

        compareLists(expected, EX07.readFriendsFromFile("longlist"));
    }

    @Test
    public void testReadFriendsFromFileLongNames() {

        String longName1 = "Adolph Blaine Charles David Earl Frederick Gerald Hubert Irvim John Kenneth Loyd Martin Nero Oliver Paul Quincy Randolph Sherman Thomas Uncas Victor Willian Xerxes Yancy Zeus Wolfeschlegelsteinhausenbergerdorffvoralternwarengewissenhaftschafers wesenchafewarenwholgepflegeundsorgfaltigkeitbeschutzenvonangereifen duchihrraubgiriigfeindewelchevorralternzwolftausendjahresvorandieer scheinenbanderersteerdeemmeshedrraumschiffgebrauchlichtalsseinu rsprungvonkraftgestartseinlangefahrthinzwischensternartigraumaufde rsuchenachdiesternwelshegehabtbewohnbarplanetenkreisedrehensichund wohinderneurassevanverstandigmenshlichkeittkonntevortpflanzenundsiche rfreunanlebenslamdlichfreudeundruhemitnichteinfurchtvorangreifenvon andererintlligentgeschopfsvonhinzwischensternartigraum";
        String longName2 = "Red Wacky League Antlez Broke the Stereo Neon Tide Bring Back Honesty Coalition Feedback Hand of Aces Keep Going Captain Let’s Pretend Lost State of Dance Paper Taxis Lunar Road Up! Down! Strange! All and I Neon Sheep Eve Hornby Faye Bradley AJ Wilde Michael Rice Dion Watts Matthew Appleyard John Ashurst Lauren Swales Zoe Angus Jaspreet Singh Emma Matthews Nicola Brown Leanne Pickering Victoria Davies Rachel Burnside Gil Parker Freya Watson Alisha Watts James Pearson Jacob Sotheran-Darley Beth Lowery Jasmine Hewitt Chloe Gibson Molly Farquhar Lewis Murphy Abbie Coulson Nick Davies Harvey Parker Kyran Williamson Michael Anderson Bethany Murray Sophie Hamilton Amy Wilkins Emma Simpson Liam Wales Jacob Bartram Alex Hooks Rebecca Miller Caitlin Miller Sean McCloskey Dominic Parker Abbey Sharpe Elena Larkin Rebecca Simpson Nick Dixon Abbie Farrelly Liam Grieves Casey Smith Liam Downing Ben Wignall Elizabeth Hann Danielle Walker Lauren Glen James Johnson Ben Ervine Kate Burton James Hudson Daniel Mayes Matthew Kitching Josh Bennett Evolution Dreams";

        List<String> names1 = Arrays.asList(longName1.split(" "));
        List<String> names2 = Arrays.asList(longName2.split(" "));

        TestFriend f1 = new TestFriend(names1.get(names1.size() - 1),
                names1.subList(0, names1.size() - 1));
        TestFriend f2 = new TestFriend(names2.get(names2.size() - 1),
                names2.subList(0, names2.size() - 1));

        List<TestFriend> expected = new ArrayList<>(Arrays.asList(f1, f2));
        compareLists(expected, EX07.readFriendsFromFile("longnames"));
    }

    @Test
    public void testFindFriendByLastNameBasic() {

        EX07.readFriendsFromFile("basicfriends");

        TestFriend f4 = new TestFriend("Longstreet", Arrays.asList("Chet"));

        Friend actual = EX07.findFriendByLastName("Longstreet");
        compareFriends(f4, actual);
    }

    @Test
    public void testFindFriendByLastNameLongName() {

        EX07.readFriendsFromFile("longnames");

        String longName = "Red Wacky League Antlez Broke the Stereo Neon Tide Bring Back Honesty Coalition Feedback Hand of Aces Keep Going Captain Let’s Pretend Lost State of Dance Paper Taxis Lunar Road Up! Down! Strange! All and I Neon Sheep Eve Hornby Faye Bradley AJ Wilde Michael Rice Dion Watts Matthew Appleyard John Ashurst Lauren Swales Zoe Angus Jaspreet Singh Emma Matthews Nicola Brown Leanne Pickering Victoria Davies Rachel Burnside Gil Parker Freya Watson Alisha Watts James Pearson Jacob Sotheran-Darley Beth Lowery Jasmine Hewitt Chloe Gibson Molly Farquhar Lewis Murphy Abbie Coulson Nick Davies Harvey Parker Kyran Williamson Michael Anderson Bethany Murray Sophie Hamilton Amy Wilkins Emma Simpson Liam Wales Jacob Bartram Alex Hooks Rebecca Miller Caitlin Miller Sean McCloskey Dominic Parker Abbey Sharpe Elena Larkin Rebecca Simpson Nick Dixon Abbie Farrelly Liam Grieves Casey Smith Liam Downing Ben Wignall Elizabeth Hann Danielle Walker Lauren Glen James Johnson Ben Ervine Kate Burton James Hudson Daniel Mayes Matthew Kitching Josh Bennett Evolution Dreams";
        List<String> names = Arrays.asList(longName.split(" "));

        TestFriend expected = new TestFriend(names.get(names.size() - 1),
                names.subList(0, names.size() - 1));

        compareFriends(expected, EX07.findFriendByLastName("Dreams"));
    }

    @Test
    public void testFindFriendByLastNameNoMatch() {
        EX07.readFriendsFromFile("basicfriends");

        assertEquals(null, EX07.findFriendByLastName("asdjajksdkasd"));
    }

    @Test
    public void testGetFullName() {
        TestFriend f = new TestFriend("Baggins",
                Arrays.asList("Adolph", "Blaine", "Charles",
                "David", "Earl", "Frederick", "Gerald",
                "Hubert", "Irvim", "John", "Kenneth", "Loyd",
                "Martin", "Nero", "Oliver", "Paul", "Quincy",
                "Randolph", "Sherman", "Thomas", "Uncas", "Victor",
                "Willian", "Xerxes", "Yancy", "Zeus"));
        String expected = "Adolph Blaine Charles David Earl Frederick Gerald Hubert Irvim John Kenneth Loyd Martin Nero Oliver Paul Quincy Randolph Sherman Thomas Uncas Victor Willian Xerxes Yancy Zeus Baggins";
        assertEquals(expected, f.getFullName());
    }

    @Test
    public void testReadFriendsFromFileNullInput() {
        List<Friend> actual = EX07.readFriendsFromFile(null);
        assertTrue(actual == null || actual.size() == 0);
    }

    @Test
    public void testfindFriendByLastNameNullInput() {
        assertEquals(null, EX07.findFriendByLastName(null));
    }

    @Test(timeout = 1000)
    public void testFindFriendByLastNameSameLastNames() {
        EX07.readFriendsFromFile("matchinglastnames");
        Friend actual = EX07.findFriendByLastName("Baggins");
        /*
Frodo Baggins
Bilbo Baggins
Guido Baggins
         */
        // check lastname
        assertEquals("Baggins", actual.getLastName());
        // check we have one firstname
        assertEquals(1, actual.getNames().size());
        String actualName = actual.getNames().get(0);
        Set<String> correctNames = new HashSet<>(
                Arrays.asList("Frodo", "Bilbo", "Guido"));
        assertTrue(correctNames.contains(actualName));
    }
    public static void compareFriends(TestFriend expected, Friend actual) {
        System.out.println(expected.getNames() + " acutal : " + actual.getNames());
        assertEquals(expected.getNames(), actual.getNames());
        assertEquals(expected.getLastName(), actual.getLastName());
    }

    public static void compareLists(List<TestFriend> expectedList, List<Friend> actualList) {
        for (int i = 0; i < expectedList.size(); i++) {
            System.out.println("Actual: " + actualList.get(i).getNames());
            System.out.println("Expected : " + expectedList.get(i).getNames());
        }
        if (expectedList.size() != actualList.size()) fail();
        for (int i = 0; i < expectedList.size(); i++) {
            compareFriends(expectedList.get(i), actualList.get(i));
        }
    }
}