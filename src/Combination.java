import java.util.Arrays;

/**
 * Created by artur on 20.03.16.
 */
public class Combination {

    int[] finalResult;
    int column;

    @Override
    public String toString() {
        return "Combination{" +
                "finalResult=" + Arrays.toString(finalResult) +
                ", column=" + column +
                ", rotation=" + rotation +
                '}';
    }

    int rotation;
}
