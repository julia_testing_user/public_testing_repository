
import org.jcp.xml.dsig.internal.dom.DOMDigestMethod;
import org.junit.Test;

import static org.junit.Assert.*;

public class BankAccountTest {

    public static double DOUBLE_PRECISION = 0.01;

    @Test(timeout = 1000)
    public void testClassExists() {
        BankAccount instance = new BankAccount();
        assertNotNull(instance);
    }

    @Test(timeout = 1000)
    public void testGetBalanceExists() {
        BankAccount instance = new BankAccount();
        instance.getBalance();
    }

    @Test (timeout = 1000)
    public void testCompiles() {
        assertTrue(true);
    }

    @Test (timeout = 1000)
    public void testCompiles2() {
        assertTrue(true);
    }

    @Test(timeout = 1000)
    public void testWithdrawMoneyExists() {
        BankAccount instance = new BankAccount();
        instance.withdrawMoney(100);
    }

    @Test(timeout = 1000)
    public void testAddMoneyExists() {
        BankAccount instance = new BankAccount();
        instance.addMoney(100);
    }


    @Test(timeout = 1000)
    public void testGetBalance() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.addMoney(100.0);

        assertEquals(100.0, bankAccount.getBalance(), DOUBLE_PRECISION);
    }

    @Test(timeout = 1000)
    public void testWithdrawMoneySimpleCase() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.addMoney(100.0);
        assertEquals(70, bankAccount.withdrawMoney(30.0), DOUBLE_PRECISION);
        assertEquals(70.0, bankAccount.getBalance(), DOUBLE_PRECISION);
    }

    @Test(timeout = 1000)
    public void testWithdrawMoneyTooMuch() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.addMoney(100.0);

        assertEquals(Double.NaN, bankAccount.withdrawMoney(101.0), DOUBLE_PRECISION);
        assertEquals(100.0, bankAccount.getBalance(), DOUBLE_PRECISION);
    }

    @Test(timeout = 1000)
    public void testTransferMoneyToSimpleCase() {
        BankAccount first = new BankAccount();
        BankAccount second = new BankAccount();

        first.addMoney(100.0);
        assertTrue(first.transferMoneyTo(second, 50.0));

        assertEquals(49.5, first.getBalance(), DOUBLE_PRECISION);
        assertEquals(50.0, second.getBalance(), DOUBLE_PRECISION);
    }

    @Test(timeout = 1000)
    public void testTransferAllMoney() {
        BankAccount first = new BankAccount();
        BankAccount second = new BankAccount();
        first.addMoney(100.0);

        assertFalse(second.transferMoneyTo(second, 100.0));
        assertEquals(100.0, first.getBalance(), DOUBLE_PRECISION);
        assertEquals(0, second.getBalance(), DOUBLE_PRECISION);
    }

    @Test(timeout = 1000)
    public void testGetMoneyTwoAccounts() {
        BankAccount first = new BankAccount();
        BankAccount second = new BankAccount();
        first.addMoney(50);
        second.addMoney(100);
        assertEquals(50, first.getBalance(), DOUBLE_PRECISION);
        assertEquals(100, second.getBalance(), DOUBLE_PRECISION);

    }

    @Test(timeout = 1000)
    public void testTransferNegativeMoney() {
        BankAccount first = new BankAccount();
        BankAccount second = new BankAccount();
        first.addMoney(100);
        second.addMoney(50);
        assertFalse(first.transferMoneyTo(second, -45));
        assertEquals(100, first.getBalance(), DOUBLE_PRECISION);
        assertEquals(50, second.getBalance(), DOUBLE_PRECISION);
    }

    @Test(timeout = 1000)
    public void testTransferToNull() {
        BankAccount first = new BankAccount();
        first.addMoney(100);
        assertFalse(first.transferMoneyTo(null, 10));
    }

    @Test(timeout = 1000)
    public void testStressfulTransactions() {
        BankAccount[] accounts = new BankAccount[100];
        for (int i = 0; i < 100; i++) {
            accounts[i] = new BankAccount();
            accounts[i].addMoney(i);
        }
        double fee = 0;
        for (int i = 0; i < 99; i++) {
            fee += accounts[i].getBalance() * 0.01;
            accounts[i].transferMoneyTo(accounts[i + 1], accounts[i].getBalance() * 0.98999);
        }
        assertEquals(fee + accounts[99].getBalance(), 4950, 5);
    }
}