public class BankAccount {
    private double addedOnePercentage = 1.01;
    private double balance;

    public boolean transferMoneyTo(BankAccount targetAccount, double amount) {
        if (targetAccount == null || (this.balance - (addedOnePercentage * amount) < 0) || amount < 0) return false;
        targetAccount.addMoney(amount);
        this.balance -= addedOnePercentage * amount;
        return true;
    }

    public double getBalance() {
        return this.balance;
    }

    public double withdrawMoney(double amount) {
        if (this.balance - amount < 0 ) return Double.NaN;
        this.balance -= amount;
        return getBalance();
    }

    public void addMoney(double amount) {
        this.balance += amount;
    }

}
