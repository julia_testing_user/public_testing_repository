
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class EX10Tests {

    public Customer getCustomer(int priority) {
        return new Customer(priority, UUID.randomUUID().toString(), UUID.randomUUID().toString());
    }

    public Package createPremiumPackage(int p1, int p2, int p3) {
        PremiumPackage p = new PremiumPackage();
        p.setSender(getCustomer(p1));
        p.setReceiver(getCustomer(p2));
        p.setPriority(p3);
        p.setWidth(10);
        p.setHeight(10);
        return p;
    }

    public Package createBasicPackage(int p1, int p2) {
        Package p = new Package();
        p.setSender(getCustomer(p1));
        p.setReceiver(getCustomer(p2));
        p.setWidth(10);
        p.setHeight(10);
        return p;
    }

    public void orderCheck(List<Package> packages) {
        Collections.shuffle(packages);
        //System.out.println(packages);
        orderCheck(true, packages);
        orderCheck(false, packages);
    }

    /**
     * Keep package number in order
     *
     * @param asc
     * @param packages
     */
    public void orderCheck(boolean asc, List<Package> packages) {
        PackageProvider packageProvider = new OrderedPackageProvider();
        if (!asc) Collections.reverse(packages);
        for (Package p : packages) {
            packageProvider.addPackage(p);
        }
        List<Package> result = new ArrayList<>();
        for (int i = 0; i < packages.size(); i++) {
            Package p = packageProvider.getNextPackage();
            result.add(p);
            assertEquals("p" + i, p.getPackageNumber());
        }
    }

    @Test(timeout = 1000)
    public void testGetReceiverPackages() {
        List<Package> packages = new ArrayList<>();
        PackageProvider packageProvider = new OrderedPackageProvider();
        Customer customer = getCustomer(100);
        for (int i = 0; i < 100; i++) {
            Package a = createBasicPackage(10, 10);
            a.setPackageNumber("asdsad" + i);
            Package b = createPremiumPackage(10, 10, 10);
            b.setPackageNumber("asdasdasd" + i);
            if (i % 10 == 0) {
                a.setReceiver(customer);
                b.setReceiver(customer);
                packages.add(b);
                packages.add(a);
            }
            packageProvider.addPackage(a);
            packageProvider.addPackage(b);
        }
        List<Package> newPackages = packageProvider.findAllPackagesByReceiver(customer);
        for (Package p : packages) {
            assertTrue(newPackages.contains(p));
        }
    }

    @Test(timeout = 1000)
    public void testGetSenderPackages() {
        List<Package> packages = new ArrayList<>();
        PackageProvider packageProvider = new OrderedPackageProvider();
        Customer customer = getCustomer(100);
        for (int i = 0; i < 100; i++) {
            Package a = createBasicPackage(10, 10);
            a.setPackageNumber("asdsad" + i);
            Package b = createPremiumPackage(10, 10, 10);
            b.setPackageNumber("asdasdasd" + i);
            if (i % 10 == 0) {
                a.setSender(customer);
                b.setSender(customer);
                packages.add(b);
                packages.add(a);
            }
            packageProvider.addPackage(a);
            packageProvider.addPackage(b);
        }
        List<Package> newPackages = packageProvider.findAllPackagesBySender(customer);
        for (Package p : packages) {
            assertTrue(newPackages.contains(p));
        }
    }

    @Test(timeout = 1000)
    public void testGetAllPackages() {
        List<Package> packages = new ArrayList<>();
        PackageProvider packageProvider = new OrderedPackageProvider();
        for (int i = 0; i < 100; i++) {
            Package a = createBasicPackage(10, 10);
            a.setPackageNumber("asdsad" + i);
            Package b = createPremiumPackage(10, 10, 10);
            b.setPackageNumber("asdasdasd" + i);
            packageProvider.addPackage(a);
            packageProvider.addPackage(b);
            packages.add(a);
            packages.add(b);
        }
        List<Package> newPackages = packageProvider.getPackages();
        for (Package p : packages) {
            assertTrue(packages.contains(p));
        }
    }

    @Test(timeout = 1000)
    public void testSetGetPackageFilter() {
        class MockFilter implements PackageFilter {
            @Override
            public boolean isValid(Package p) {
                return false;
            }
        }
        MockFilter a = new MockFilter();
        PackageProvider b = new OrderedPackageProvider();
        b.setPackageFilter(a);
        assertEquals(a, b.getPackageFilter());
    }

    @Test(timeout = 1000)
    public void testSetGetPackageProvider() {
        class MockProvider implements PackageProvider {

            @Override
            public Package getNextPackage() {
                return null;
            }

            @Override
            public void addPackage(Package packageToAdd) {

            }

            @Override
            public boolean hasNextPackage() {
                return false;
            }

            @Override
            public void setPackageFilter(PackageFilter packageFilter) {

            }

            @Override
            public PackageFilter getPackageFilter() {
                return null;
            }

            @Override
            public void setBrokenPackageProvider(PackageProvider packageProvider) {

            }

            @Override
            public PackageProvider getBrokenPackageProvider() {
                return null;
            }

            @Override
            public List<Package> getPackages() {
                return null;
            }

            @Override
            public List<Package> findAllPackagesBySender(Customer customer) {
                return null;
            }

            @Override
            public List<Package> findAllPackagesByReceiver(Customer customer) {
                return null;
            }
        }
        MockProvider a = new MockProvider();
        PackageProvider b = new OrderedPackageProvider();
        b.setBrokenPackageProvider(a);
        assertEquals(a, b.getBrokenPackageProvider());
    }

    @Test(timeout = 1000)
    public void testInsertOnePackageGetOnePackage() {
        PackageProvider packageProvider = new OrderedPackageProvider();
        Package p = createBasicPackage(10, 10);
        packageProvider.addPackage(p);
        assertEquals(packageProvider.getNextPackage(), p);
    }

    @Test(timeout = 1000)
    public void testGetPackageFromEmptyProvider() {
        PackageProvider packageProvider = new OrderedPackageProvider();
        packageProvider.getNextPackage();
    }

    @Test(timeout = 1000)
    public void testHasNext() {
        Package p = createBasicPackage(10, 10);
        p.setPackageNumber("p0");
        PackageProvider packageProvider = new OrderedPackageProvider();
        assertFalse(packageProvider.hasNextPackage());
        packageProvider.addPackage(p);
        assertTrue(packageProvider.hasNextPackage());

    }

    @Test(timeout = 1000)
    public void testPremiumAndBasicTogetherOnlyPremiumPriorityCounts() {
        List<Package> packages = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Package p = createPremiumPackage(1, 1, 1 + 9 - i);
            p.setPackageNumber("p" + i);
            packages.add(p);
        }
        for (int i = 10; i < 20; i++) {
            Package p = createBasicPackage(100 + (9 - i), 100 + (9 - i));
            p.setPackageNumber("p" + i);
            packages.add(p);
        }
        orderCheck(packages);
    }

    @Test(timeout = 1000)
    public void testPremiumPackagesSamePriorityCustomerPriorityChanges() {
        List<Package> packages = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Package p = createPremiumPackage(1, 1, 1 + 9 - i);
            p.setPackageNumber("p" + i);
            packages.add(p);
        }
        orderCheck(packages);
    }

    @Test(timeout = 1000)
    public void testOnlyPremiumPackageRetrievalOrderCustomersSamePriority() {
        List<Package> packages = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Package p = createPremiumPackage(1, 1, 1 + 9 - i);
            p.setPackageNumber("p" + i);
            packages.add(p);
        }
        orderCheck(packages);
    }

    @Test(timeout = 1000)
    public void testOnlyBasicPackageRetrievalOrder() {
        List<Package> packages = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Package p = createBasicPackage(1 + 9 - i, 1 + 9 - i);
            p.setPackageNumber("p" + i);
            packages.add(p);
        }
        orderCheck(packages);
    }

    @Test(timeout = 1000)
    public void testAddSamePackageInstance() throws Exception {
        Package pp = createPremiumPackage(10, 10, 10);
        pp.setPackageNumber("p0");
        PackageProvider packageProvider = new OrderedPackageProvider();
        packageProvider.addPackage(pp);
        packageProvider.addPackage(pp);
        packageProvider.addPackage(pp);
        assertEquals(pp, packageProvider.getNextPackage());
        assertFalse(packageProvider.hasNextPackage());
    }

    @Test(timeout = 1000)
    public void testAddDifferentPackageInstanceWithSameValues() throws Exception {
        PremiumPackage pp = (PremiumPackage) createPremiumPackage(10, 10, 10);
        pp.setPackageNumber("p0");
        PremiumPackage pp2 = new PremiumPackage();
        pp2.setSender(pp.getSender());
        pp2.setReceiver(pp.getReceiver());
        pp2.setPriority(pp.getPriority());
        pp2.setWidth(pp.getWidth());
        pp2.setHeight(pp.getHeight());
        pp2.setPackageNumber(pp.getPackageNumber());

        PackageProvider packageProvider = new OrderedPackageProvider();
        packageProvider.addPackage(pp);
        packageProvider.addPackage(pp2);

        for (int i = 0; i < 2; i++) {
            PremiumPackage p = (PremiumPackage) packageProvider.getNextPackage();
            assertEquals(pp.getHeight(), p.getHeight());
            assertEquals(pp.getWidth(), p.getWidth());
            assertEquals(pp.getPriority(), p.getPriority());
            assertEquals(pp.getPackageNumber(), p.getPackageNumber());
            assertEquals(pp.getSender().getName(), p.getSender().getName());
            assertEquals(pp.getSender().getAddress(), p.getSender().getAddress());
            assertEquals(pp.getSender().getAddress(), p.getSender().getAddress());
        }

        assertFalse(packageProvider.hasNextPackage());
    }
}