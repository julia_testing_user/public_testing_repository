import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

/**
 *
 */
public class OrderedPackageProvider implements PackageProvider {

    PriorityQueue<Package> queue = new PriorityQueue<>();
    PackageFilter packageFilter;
    PackageProvider brokenPackageProvider;

    @Override
    public Package getNextPackage() {
        return queue.poll();
    }

    @Override
    public void addPackage(Package packageToAdd) {
        boolean check = true;
        if (packageFilter != null) {
            try {
                check = packageFilter.isValid(packageToAdd);
                if (!check && brokenPackageProvider != null) {
                    brokenPackageProvider.addPackage(packageToAdd);
                }
            } catch (Exception e) {}
        }

        if (!queue.contains(packageToAdd) && check) queue.add(packageToAdd);
    }

    @Override
    public boolean hasNextPackage() {
        return queue.size() > 0;
    }

    @Override
    public void setPackageFilter(PackageFilter packageFilter) {
        this.packageFilter = packageFilter;
    }

    @Override
    public PackageFilter getPackageFilter() {
        return packageFilter;
    }

    @Override
    public void setBrokenPackageProvider(PackageProvider packageProvider) {
        brokenPackageProvider = packageProvider;
    }

    @Override
    public PackageProvider getBrokenPackageProvider() {
        return brokenPackageProvider;
    }

    @Override
    public List<Package> getPackages() {
        return queue.stream().collect(Collectors.toList());
    }

    @Override
    public List<Package> findAllPackagesBySender(Customer customer) {
        return queue.stream().filter(x -> x.getSender() == customer).collect(Collectors.toList());
    }

    @Override
    public List<Package> findAllPackagesByReceiver(Customer customer) {
        return queue.stream().filter(x -> x.getReceiver() == customer).collect(Collectors.toList());
    }
}
