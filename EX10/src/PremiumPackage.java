public class PremiumPackage extends Package implements Comparable<Package> {

    private int priority;

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public PremiumPackage() {
    }

    public PremiumPackage(int priority, String packageNumber, int width, int height) {
        super(packageNumber, width, height);
        this.priority = priority;
    }

    @Override
    public int compareTo(Package aPackage) {
        if (!(aPackage instanceof PremiumPackage)) {
            return -1;
        } else {
            if (getPriority() == ((PremiumPackage) aPackage).getPriority()) {
                return aPackage.getCustomerPriority() - getCustomerPriority();
            } else {
                return ((PremiumPackage) aPackage).getPriority() - getPriority();
            }
        }
    }

    @Override
    public String toString() {
        return getPackageNumber() + "(premium)";
    }
}
