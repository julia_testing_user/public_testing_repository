import java.util.List;

public class OrderedPackageFilter implements PackageFilter {

    public boolean isValid(Package p) {

        if (p == null) return false;
        // Receiver, sender ei ole tühi, receiveril on nimi (mitte tühistring).
        if (p.getSender() == null) return false;
        if (p.getPackageNumber() == null) return false;
        if(p.getPackageNumber().trim().isEmpty()) return false;
        if (p.getReceiver() == null) return false;
        if (p.getReceiver().getName() == null) return false;
        if (p.getReceiver().getName().trim().isEmpty()) return false;
        if (p.getSender().getName() == null) return false;
        if (p.getSender().getName().trim().isEmpty()) return false;
        // Receiver, sender aadress ei ole tühi
        if (p.getSender().getAddress() == null) return false;
        if (p.getSender().getAddress().trim().isEmpty()) return false;
        if (p.getReceiver().getAddress() == null) return false;
        if (p.getReceiver().getAddress().trim().isEmpty()) return false;
        // Receiver, sender priority on suurem kui 0 ja väiksem kui 1000
        if (p.getSender().getPriority() <= 0) return false;
        if (p.getSender().getPriority() >= 1000) return false;
        if (p.getReceiver().getPriority() <= 0) return false;
        if (p.getReceiver().getPriority() >= 1000) return false;
        // Kõrgus ja laius on rangelt 0-st suuremad ja rangelt 1000-st väiksemad
        if (p.getWidth() <= 0) return false;
        if (p.getWidth() >= 1000) return false;
        if (p.getHeight() <= 0) return false;
        if (p.getHeight() >= 1000) return false;
        // Receiver ei ühti senderiga (kliendi andmed peavad erinema)
        if (p.getSender().getName().trim().equals(p.getReceiver().getName().trim()) &&
                p.getSender().getAddress().trim().equals(p.getReceiver().getAddress().trim()) &&
                p.getSender().getPriority() == p.getReceiver().getPriority())
            return false;

        // PremiumPackage puhul:

        // Priority rangelt suurem kui 0 ja rangelt väiksem kui 1000.
        if (p instanceof PremiumPackage) {
            PremiumPackage pp = (PremiumPackage) p;
            if (pp.getPriority() <= 0) return false;
            if (pp.getPriority() >= 1000) return false;
        }
        // PackageProvideris ei tohi olla 2 korda sama pakki (paki andmed peavad erinema)

        return true;
    }
}
