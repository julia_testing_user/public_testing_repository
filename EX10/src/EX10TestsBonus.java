
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static org.junit.Assert.*;

public class EX10TestsBonus {


    class MockProvider implements PackageProvider {
        int cnt = 0;

        int getCount() {
            return cnt;
        }
        @Override
        public Package getNextPackage() {
            return null;
        }

        @Override
        public void addPackage(Package packageToAdd) {
            cnt++;
        }

        @Override
        public boolean hasNextPackage() {
            return false;
        }

        @Override
        public void setPackageFilter(PackageFilter packageFilter) {

        }

        @Override
        public PackageFilter getPackageFilter() {
            return null;
        }

        @Override
        public void setBrokenPackageProvider(PackageProvider packageProvider) {

        }

        @Override
        public PackageProvider getBrokenPackageProvider() {
            return null;
        }

        @Override
        public List<Package> getPackages() {
            return null;
        }

        @Override
        public List<Package> findAllPackagesBySender(Customer customer) {
            return null;
        }

        @Override
        public List<Package> findAllPackagesByReceiver(Customer customer) {
            return null;
        }
    }
    private PackageFilter filter;
    private List<Package> packages;
    private PackageProvider provider;

    @Before
    public void setUp() throws Exception {
        filter = new OrderedPackageFilter();
        packages = new ArrayList<Package>();
    }

    public Package createPerfectPackage() {

        Customer receiver = new Customer();
        Customer sender = new Customer();
        receiver.setAddress("address1");
        receiver.setName("name1");
        receiver.setPriority(100);

        sender.setAddress("address2");
        sender.setName("name2");
        sender.setPriority(101);

        Package p = new Package();
        p.setReceiver(receiver);
        p.setSender(sender);
        p.setHeight(100);
        p.setWidth(100);
        p.setPackageNumber("Number10");
        return p;
    }

    @Test(timeout = 1000)
    public void testSenderNull() {
        Package p = createPerfectPackage();
        p.setSender(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverMissing() {
        Package p = createPerfectPackage();
        p.setReceiver(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverNameNull() {
        Package p = createPerfectPackage();
        p.getReceiver().setName(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderNameNull() {
        Package p = createPerfectPackage();
        p.getSender().setName(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverNameEmptyString() {
        Package p = createPerfectPackage();
        p.getReceiver().setName("");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverNameEmptyString2() {
        Package p = createPerfectPackage();
        p.getReceiver().setName("              ");
        assertFalse(filter.isValid(p));
    }


    @Test(timeout = 1000)
    public void testSenderNameEmptyString() {
        Package p = createPerfectPackage();
        p.getSender().setName("");
        assertFalse(filter.isValid(p));
    }


    @Test(timeout = 1000)
    public void testSenderNameEmptyString2() {
        Package p = createPerfectPackage();
        p.getSender().setName("                ");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverAddressNull() {
        Package p = createPerfectPackage();
        p.getReceiver().setAddress(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverAddressEmptyString() {
        Package p = createPerfectPackage();
        p.getReceiver().setAddress("");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverAddressEmptyString2() {
        Package p = createPerfectPackage();
        p.getReceiver().setAddress("               ");
        assertFalse(filter.isValid(p));
    }


    @Test(timeout = 1000)
    public void testSenderAddressNull() {
        Package p = createPerfectPackage();
        p.getSender().setAddress(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderAddressEmptyString() {
        Package p = createPerfectPackage();
        p.getSender().setAddress("");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderAddressEmptyString2() {
        Package p = createPerfectPackage();
        p.getSender().setAddress("               ");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverPriorityZero() {
        Package p = createPerfectPackage();
        p.getReceiver().setPriority(0);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverPriority1000() {
        Package p = createPerfectPackage();
        p.getReceiver().setPriority(1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverPriorityNegative() {
        Package p = createPerfectPackage();
        p.getReceiver().setPriority(-1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderPriorityZero() {
        Package p = createPerfectPackage();
        p.getSender().setPriority(0);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderPriority1000() {
        Package p = createPerfectPackage();
        p.getSender().setPriority(1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderPriorityNegative() {
        Package p = createPerfectPackage();
        p.getSender().setPriority(-1000);
        assertFalse(filter.isValid(p));
    }


    @Test(timeout = 1000)
    public void testPackageNumberNull() {
        Package p = createPerfectPackage();
        p.setPackageNumber(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageNumberEmpty() {
        Package p = createPerfectPackage();
        p.setPackageNumber("");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageNumberEmpty2() {
        Package p = createPerfectPackage();
        p.setPackageNumber("          ");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageHeight1000() {
        Package p = createPerfectPackage();
        p.setHeight(1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageHeightNegative() {
        Package p = createPerfectPackage();
        p.setHeight(-1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageHeight0() {
        Package p = createPerfectPackage();
        p.setHeight(0);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageWidth1000() {
        Package p = createPerfectPackage();
        p.setWidth(1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageWidthNegative() {
        Package p = createPerfectPackage();
        p.setWidth(-1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageWidth0() {
        Package p = createPerfectPackage();
        p.setWidth(0);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer1InstaneCheck() {
        Package p = createPerfectPackage();
        p.setSender(p.getReceiver());
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer2PriorityCheck() {
        Package p = createPerfectPackage();
        Customer customer2 = new Customer();
        customer2.setPriority(p.getSender().getPriority() + 1);
        customer2.setAddress(p.getSender().getAddress());
        customer2.setName(p.getSender().getName());
        p.setReceiver(customer2);
        assertTrue(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer3AddressCheck() {
        Package p = createPerfectPackage();
        Customer customer2 = new Customer();
        customer2.setPriority(p.getSender().getPriority());
        customer2.setAddress(p.getSender().getAddress() + " and ");
        customer2.setName(p.getSender().getName());
        p.setReceiver(customer2);
        assertTrue(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer4NameCheck() {
        Package p = createPerfectPackage();
        Customer customer2 = new Customer();
        customer2.setPriority(p.getSender().getPriority());
        customer2.setAddress(p.getSender().getAddress());
        customer2.setName(p.getSender().getName() + " and ");
        p.setReceiver(customer2);
        assertTrue(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer5SameDataDifferentInstanceCheck() {
        Package p = createPerfectPackage();
        Customer customer2 = new Customer();
        customer2.setPriority(p.getSender().getPriority());
        customer2.setAddress(p.getSender().getAddress());
        customer2.setName(p.getSender().getName());
        p.setReceiver(customer2);
        assertFalse(filter.isValid(p));
    }

    //@Test(timeout = 1000)
    //
    public void testPackageSameCustomer6SameNameExtraSpaces() {
        Package p = createPerfectPackage();
        Customer customer2 = new Customer();
        customer2.setPriority(p.getSender().getPriority());
        customer2.setAddress(p.getSender().getAddress());
        customer2.setName(p.getSender().getName() + "        ");
        p.setReceiver(customer2);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testCorrectPackage() {
        Package p = createPerfectPackage();
        assertTrue(filter.isValid(p));
    }

    private void testRandomPackages(int countBasic, int countPremium) {
        PackageProvider provider = new OrderedPackageProvider();
        PackageFilter filter = new OrderedPackageFilter();
        provider.setPackageFilter(filter);
        MockProvider trash = new MockProvider();
        provider.setBrokenPackageProvider(trash);
        Random rnd = new Random();
        String[] packageNumbers = {"  ", "", null, "pakk", "pakk1", "pakk2", "pakk3"};
        int validPackagesCount = 0;
        int invalidPackagesCount = 0;
        boolean errorShown = false; // show only the first problem
        for (int i = 0; i < countBasic + countPremium; i++) {
            boolean valid = true;
            int packageNumberIndex = rnd.nextInt(packageNumbers.length);
            int width = rnd.nextInt(100) - 1;
            int height = rnd.nextInt(100) + 902;
            Package p;
            if (i < countBasic) {
                p = new Package(packageNumbers[packageNumberIndex], width, height);
            } else {
                int priority = rnd.nextInt(1002) - 1;
                if (priority < 1 || priority > 999) valid = false;
                p = new PremiumPackage(priority, packageNumbers[packageNumberIndex], width, height);
            }
            if (packageNumberIndex < 3) valid = false;
            if (width < 1) valid = false;
            if (height > 999) valid = false;

            String[] customerNames = {null, "", "   ", "mati", "kati", "jaakup", "rein",
            "MATI", "KALA", "süsteem", "mingi", "suvline", "nimi", "veel äkki?"};
            String[] addresses = {null, "", "            ", "tallinn", "pärnu",
            "eesti", "poe taga", "jaagu juures", "korter 2", "korter 1-2", "elavhõbeda",
            "kuu", "päike", "vändra", "narva", "moskva"};
            Customer sender;
            Customer receiver;
            if (rnd.nextInt(100) == 0) {
                sender = null;
                valid = false;
            } else {
                int priority = rnd.nextInt(100);
                if (priority < 1) valid = false;
                int customerNameIndex = rnd.nextInt(customerNames.length);
                if (customerNameIndex < 3) valid = false;
                int addressIndex = rnd.nextInt(addresses.length);
                if (addressIndex < 3) valid = false;

                sender = new Customer(priority,
                        customerNames[customerNameIndex],
                        addresses[addressIndex]);

            }
            if (rnd.nextInt(100) == 0) {
                valid = false;
                receiver = null;
            } else {
                int priority = rnd.nextInt(100);
                if (priority < 1) valid = false;
                int customerNameIndex = rnd.nextInt(customerNames.length);
                if (customerNameIndex < 3) valid = false;
                int addressIndex = rnd.nextInt(addresses.length);
                if (addressIndex < 3) valid = false;

                receiver = new Customer(priority,
                        customerNames[customerNameIndex],
                        addresses[addressIndex]);
            }
            p.setSender(sender);
            p.setReceiver(receiver);

            if (sender != null && receiver != null) {
                if (Objects.equals(sender.getName(), receiver.getName()) &&
                        Objects.equals(sender.getAddress(), receiver.getAddress()) &&
                        sender.getPriority() == receiver.getPriority())
                    valid = false;
            }
            boolean resultValid = filter.isValid(p);
            if (!errorShown && valid ^ resultValid) {
                errorShown = true;
                System.out.println("test:" + valid);
                System.out.println("code:" + resultValid);
                if (p instanceof PremiumPackage) {
                    System.out.println("premium " + ((PremiumPackage) p).getPriority());
                }
                System.out.println(p.getPackageNumber()
                + " " + p.getWidth() + " " + p.getHeight());
                Customer s = p.getSender();
                System.out.print("sender:");
                if (s != null) {
                    System.out.println(s.getPriority() + " n:" + s.getName() + " a:" + s.getAddress());
                } else {
                    System.out.println("null");
                }
                Customer r = p.getReceiver();
                System.out.print("receiver:");
                if (s != null) {
                    System.out.println(r.getPriority() + " n:" + r.getName() + " a:" + r.getAddress());
                } else {
                    System.out.println("null");
                }
                System.out.println();
            }
            if (valid) {
                validPackagesCount++;
            } else {
                invalidPackagesCount++;
            }
            provider.addPackage(p);
        }

        System.out.println("random with " + countBasic + " basic and " + countPremium
        + ": " + validPackagesCount + " valid and " + invalidPackagesCount + " invalid packages");
        assertEquals(validPackagesCount, provider.getPackages().size());
        assertEquals(invalidPackagesCount, trash.getCount());
        System.out.println("All OK");

    }
    @Test(timeout = 1000)
    public void testPackageProviderWithFilter() throws Exception {
        PackageProvider provider = new OrderedPackageProvider();
        PackageFilter filter = new OrderedPackageFilter();
        provider.setPackageFilter(filter);
        Customer c1 = new Customer(10, "mati", "tallinn");
        Customer c2 = new Customer(10, "kati", "tallinn");
        Package p1 = new Package("valid", 10, 10);
        p1.setSender(c1);
        p1.setReceiver(c2);
        provider.addPackage(p1);
        Package p2 = new Package("invalid", 10, 10);
        p2.setSender(c1);
        provider.addPackage(p2);
        assertEquals(1, provider.getPackages().size());
    }

    @Test(timeout = 1000)
    public void testRandom20PackagesWithFilterAndTrash() throws Exception {
        testRandomPackages(10, 10);
    }
    @Test(timeout = 1000)
    public void testRandom10BasicPackagesWithFilterAndTrash() throws Exception {
        testRandomPackages(10, 0);
    }
    @Test(timeout = 1000)
    public void testRandom10PremiumPackagesWithFilterAndTrash() throws Exception {
        testRandomPackages(0, 10);
    }
    @Test(timeout = 2000)
    public void testRanom10000PackagesWithFilterAndTrash() throws Exception {
        testRandomPackages(5000, 5000);
    }
    @Test(timeout = 30000)
    public void testRanom1000000PackagesWithFilterAndTrash() throws Exception {
        testRandomPackages(50000, 50000);
    }

    @Test(timeout = 1000)
    public void testCustomFilterThrowsException() throws Exception {
        // should not give exception
        PackageFilter badFilter = new PackageFilter() {
            @Override
            public boolean isValid(Package p) {
                throw new ArrayIndexOutOfBoundsException("jama");
            }
        };
        PackageProvider provider = new OrderedPackageProvider();
        provider.setPackageFilter(badFilter);
        Package p = new Package("tere", 10, 10 );
        provider.addPackage(p);
    }

    @Test(timeout = 1000)
    public void testCustomBrokenPackageProviderThrowsException() throws Exception {
        class BadProvider extends MockProvider {
            @Override
            public void addPackage(Package packageToAdd) {
                throw new ArrayIndexOutOfBoundsException("jama jälle");
            }
        }
        PackageProvider badProvider = new BadProvider();
        PackageFilter goodFilter = new PackageFilter() {
            @Override
            public boolean isValid(Package p) {
                return false;
            }
        };

        PackageProvider provider = new OrderedPackageProvider();
        provider.setPackageFilter(goodFilter);
        provider.setBrokenPackageProvider(badProvider);
        Package p = new Package("tere", 10, 10 );
        provider.addPackage(p);
    }

    @Test(timeout = 1000)
    public void testCustomFilterOnlyTrue() throws Exception {
        PackageFilter trueFilter = new PackageFilter() {
            @Override
            public boolean isValid(Package p) {
                return true;
            }
        };
        PackageProvider provider = new OrderedPackageProvider();
        provider.setPackageFilter(trueFilter);
        Package p = new Package("tere", 10, 10);
        provider.addPackage(p);
        Package p2 = new Package(null, -10, 100);
        provider.addPackage(p2);
        assertEquals(2, provider.getPackages().size());
    }
    @Test(timeout = 1000)
    public void testCustomFilterOnlyFalse() throws Exception {
        PackageFilter falseFilter = new PackageFilter() {
            @Override
            public boolean isValid(Package p) {
                return false;
            }
        };
        PackageProvider provider = new OrderedPackageProvider();
        provider.setPackageFilter(falseFilter);
        Package p = new Package("tere", 10, 10);
        provider.addPackage(p);
        Package p2 = new Package(null, -10, 100);
        provider.addPackage(p2);
        assertEquals(0, provider.getPackages().size());
    }

    @Test(timeout = 3000)
    public void testOrderedPackageProviderAsBrokenPackageProvider() throws Exception {
        Random rnd = new Random();
        PackageFilter randomFilter = new PackageFilter() {
            @Override
            public boolean isValid(Package p) {
                return rnd.nextBoolean();
            }
        };
        PackageProvider provider = new OrderedPackageProvider();
        provider.setPackageFilter(randomFilter);
        PackageProvider trash = new OrderedPackageProvider();
        provider.setBrokenPackageProvider(trash);
        for (int i = 0; i < 10000; i++) {
            Package p;
            if (rnd.nextBoolean()) {
                p = new PremiumPackage(rnd.nextInt(1000), "pakk", 10, 10);
            } else {
                p = new Package("pakktava", 11, 11);
            }
            provider.addPackage(p);
        }
        System.out.println(provider.getPackages().size() + "+" + trash.getPackages().size());
        assertEquals(10000, provider.getPackages().size() + trash.getPackages().size());
    }
}
