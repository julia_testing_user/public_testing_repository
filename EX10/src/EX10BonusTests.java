import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class EX10BonusTests {


    private PackageFilter filter;
    private List<Package> packages;

    @Before
    public void setUp() throws Exception {
        filter = new OrderedPackageFilter();
        packages = new ArrayList<Package>();
    }

    public Package createPerfectPackage() {

        Customer receiver = new Customer();
        Customer sender = new Customer();
        receiver.setAddress("address1");
        receiver.setName("name1");
        receiver.setPriority(100);

        sender.setAddress("address2");
        sender.setName("name2");
        sender.setPriority(101);

        Package p = new Package();
        p.setReceiver(receiver);
        p.setSender(sender);
        p.setHeight(100);
        p.setWidth(100);
        p.setPackageNumber("Number10");
        return p;
    }

    @Test(timeout = 1000)
    public void testSenderNull() {
        Package p = createPerfectPackage();
        p.setSender(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverMissing() {
        Package p = createPerfectPackage();
        p.setReceiver(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverNameNull() {
        Package p = createPerfectPackage();
        p.getReceiver().setName(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderNameNull() {
        Package p = createPerfectPackage();
        p.getSender().setName(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverNameEmptyString() {
        Package p = createPerfectPackage();
        p.getReceiver().setName("");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverNameEmptyString2() {
        Package p = createPerfectPackage();
        p.getReceiver().setName("              ");
        assertFalse(filter.isValid(p));
    }


    @Test(timeout = 1000)
    public void testSenderNameEmptyString() {
        Package p = createPerfectPackage();
        p.getSender().setName("");
        assertFalse(filter.isValid(p));
    }


    @Test(timeout = 1000)
    public void testSenderNameEmptyString2() {
        Package p = createPerfectPackage();
        p.getSender().setName("                ");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverAddressNull() {
        Package p = createPerfectPackage();
        p.getReceiver().setAddress(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverAddressEmptyString() {
        Package p = createPerfectPackage();
        p.getReceiver().setAddress("");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverAddressEmptyString2() {
        Package p = createPerfectPackage();
        p.getReceiver().setAddress("               ");
        assertFalse(filter.isValid(p));
    }


    @Test(timeout = 1000)
    public void testSenderAddressNull() {
        Package p = createPerfectPackage();
        p.getSender().setAddress(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderAddressEmptyString() {
        Package p = createPerfectPackage();
        p.getSender().setAddress("");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderAddressEmptyString2() {
        Package p = createPerfectPackage();
        p.getSender().setAddress("               ");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverPriorityZero() {
        Package p = createPerfectPackage();
        p.getReceiver().setPriority(0);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverPriority1000() {
        Package p = createPerfectPackage();
        p.getReceiver().setPriority(1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testReceiverPriorityNegative() {
        Package p = createPerfectPackage();
        p.getReceiver().setPriority(-1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderPriorityZero() {
        Package p = createPerfectPackage();
        p.getSender().setPriority(0);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderPriority1000() {
        Package p = createPerfectPackage();
        p.getSender().setPriority(1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testSenderPriorityNegative() {
        Package p = createPerfectPackage();
        p.getSender().setPriority(-1000);
        assertFalse(filter.isValid(p));
    }


    @Test(timeout = 1000)
    public void testPackageNumberNull() {
        Package p = createPerfectPackage();
        p.setPackageNumber(null);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageNumberEmpty() {
        Package p = createPerfectPackage();
        p.setPackageNumber("");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageNumberEmpty2() {
        Package p = createPerfectPackage();
        p.setPackageNumber("          ");
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageHeight1000() {
        Package p = createPerfectPackage();
        p.setHeight(1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageHeightNegative() {
        Package p = createPerfectPackage();
        p.setHeight(-1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageHeight0() {
        Package p = createPerfectPackage();
        p.setHeight(0);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageWidth1000() {
        Package p = createPerfectPackage();
        p.setWidth(1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageWidthNegative() {
        Package p = createPerfectPackage();
        p.setWidth(-1000);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageWidth0() {
        Package p = createPerfectPackage();
        p.setWidth(0);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer1InstaneCheck() {
        Package p = createPerfectPackage();
        p.setSender(p.getReceiver());
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer2PriorityCheck() {
        Package p = createPerfectPackage();
        Customer customer2 = new Customer();
        customer2.setPriority(p.getSender().getPriority() + 1);
        customer2.setAddress(p.getSender().getAddress());
        customer2.setName(p.getSender().getName());
        p.setReceiver(customer2);
        assertTrue(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer3AddressCheck() {
        Package p = createPerfectPackage();
        Customer customer2 = new Customer();
        customer2.setPriority(p.getSender().getPriority());
        customer2.setAddress(p.getSender().getAddress() + " and ");
        customer2.setName(p.getSender().getName());
        p.setReceiver(customer2);
        assertTrue(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer4NameCheck() {
        Package p = createPerfectPackage();
        Customer customer2 = new Customer();
        customer2.setPriority(p.getSender().getPriority());
        customer2.setAddress(p.getSender().getAddress());
        customer2.setName(p.getSender().getName() + " and ");
        p.setReceiver(customer2);
        assertTrue(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer5SameDataDifferentInstanceCheck() {
        Package p = createPerfectPackage();
        Customer customer2 = new Customer();
        customer2.setPriority(p.getSender().getPriority());
        customer2.setAddress(p.getSender().getAddress());
        customer2.setName(p.getSender().getName());
        p.setReceiver(customer2);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testPackageSameCustomer6SameNameExtraSpaces() {
        Package p = createPerfectPackage();
        Customer customer2 = new Customer();
        customer2.setPriority(p.getSender().getPriority());
        customer2.setAddress(p.getSender().getAddress());
        customer2.setName(p.getSender().getName() + "        ");
        p.setReceiver(customer2);
        assertFalse(filter.isValid(p));
    }

    @Test(timeout = 1000)
    public void testCorrectPackage() {
        Package p = createPerfectPackage();
        Assert.assertTrue(filter.isValid(p));
    }
}
