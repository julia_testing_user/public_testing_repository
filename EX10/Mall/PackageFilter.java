/**
 * Package filtering interface.
 */
public interface PackageFilter {

    /**
     * Validate package information.
     *
     * @return Whether the package information is correct.
     */
    boolean isValid();

    /**
     * Get broken package provider.
     * Broken package provider includes all
     *
     * @return BrokenPackageProvider
     */
    PackageProvider getBrokenPackageProvider();

}
