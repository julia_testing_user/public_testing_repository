import com.sun.org.apache.xpath.internal.SourceTree;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Homework 02 - Droptris AI.
 * https://courses.cs.ttu.ee/pages/ITI0011:HW02_Droptris
 */
public class HW02 {
    /**
     * The main method. You can use this to initialize the game.
     * Tester will not execute the main method.
     *
     * @param args Arguments from command line
     */
    public static void main(String[] args) {

        for (int i = 1; i < 10; i += 2) {
            if (i % 3 == 0) {
                i--;
                System.out.println(i);
            }
        }

    }

    /**
     * Optional setup. This method will be called
     * before the game is started. You can do some
     * precalculations here if needed.
     * <p>
     * If you don't need to precalculate anything,
     * just leave it empty.
     */
    public static void setup() {
    }

    /**
     * The method to execute your AI.
     *
     * @param connectionString JSON-formatted connection string.
     *                         If you implement Socket connection yourself
     *                         you should use this string directly when connecting.
     *                         If you use DroptrisConnection, you can ignore that.
     * @return The final score. You should read the score from the server.
     */
    public static int run(String connectionString) {
        try {
            // false - only "O" blocks are sent
            DroptrisConnection conn = new DroptrisConnection("artur.luik", true);
            conn.readLine();
            int[] heightMap = new int[10];
            char nextBlock = readNextBlockInfo(conn);
            for (int i = 0; i < 1000; i++) {
                System.out.println("Next : " + nextBlock);
                Combination comb = findPlace(nextBlock, heightMap);
                putNext(comb.column, comb.rotation, conn);
                heightMap = comb.finalResult;
                nextBlock = readNextBlockInfo(conn);
                printCurrentState(conn);
            }

            // you should implement score parsing
            // here we just return 400
            return 400;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static char readNextBlockInfo(DroptrisConnection conn) throws IOException {
        return conn.readLine().charAt(11);
    }

    public static void putNext(int column, int roation, DroptrisConnection conn) throws IOException {
        conn.sendAction("{\"column\": " + column + ", \"rotation\": " + roation + " }");
    }

    public static int evalute(int[] heightMap) {

        int maximum = Integer.MIN_VALUE;
        for (int i = 0; i < 10; i++) {
            maximum = Math.max(heightMap[i], maximum);
        }
        int minimum = Integer.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            minimum = Math.min(minimum, heightMap[i]);
        }

        return minimum - maximum;
    }

    public static ArrayList<Combination> findCombinations(int[] heightMap, char next) {

        ArrayList<Combination> combinations = new ArrayList<>();

        if (next == 'O') {
            for (int i = 0; i < 9; i++) {
                int[] copy = heightMap.clone();
                int max = Math.max(copy[i], copy[i + 1]);
                copy[i] = max + 2;
                copy[i + 1] = max + 2;
                Combination comb = new Combination();
                comb.column = i;
                comb.rotation = 0;
                comb.finalResult = copy;
                combinations.add(comb);
            }
        }
        if (next == 'I') {
            // Rotation 0
            for (int i = 0; i < 10; i++) {
                int[] copy = heightMap.clone();
                copy[i] += 4;
                Combination comb = new Combination();
                comb.column = i;
                comb.rotation = 0;
                comb.finalResult = copy;
                combinations.add(comb);
            }
            // Rotation 1
            for (int i = 0; i < 6; i++) {
                int[] copy = heightMap.clone();
                int max = Math.max(Math.max(Math.max(copy[i], copy[i + 1]), copy[i + 2]), copy[i + 3]);
                copy[i] = max + 1;
                copy[i + 1] = max + 1;
                copy[i + 2] = max + 1;
                copy[i + 3] = max + 1;
                Combination comb = new Combination();
                comb.column = i;
                comb.rotation = 1;
                comb.finalResult = copy;
                combinations.add(comb);
            }
        }

        return combinations;
    }

    public static Combination findPlace(char next, int[] heightMap) {

        List<Combination> combinations = findCombinations(heightMap, next);
        int bestValue = Integer.MIN_VALUE;
        Combination bestCombination = null;

        for (int i = 0; i < combinations.size(); i++) {
            int currentValue = evalute(combinations.get(i).finalResult);
            if (bestValue < currentValue) {
                bestValue = currentValue;
                bestCombination = combinations.get(i);
            }

        }

        System.out.println(bestCombination);

        return bestCombination;
    }

    public static void printCurrentState(DroptrisConnection conn) throws IOException {
        conn.sendAction("{\"parameter\": \"state\"}");
        String result = "";
        for (int i = 0; i < 7; i++) {
            result += conn.readLine();
        }
        String m = result.substring(32, result.length() - 1);
        System.out.println(m.replace("],", "\n").replace("[", "").replace("]", "").replace(" ", "").replace(",", "").replace("0", " "));
        System.out.println(conn.readScoreData());

    }
}
