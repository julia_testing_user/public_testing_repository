/**
 * Template for HW01: Treasurehunt.
 * More information:
 * https://courses.cs.ttu.ee/pages/ITI0011:Aardejaht
 */
public class HW01 {
    /**
     * Value to return in makeMove in case
     * the cell was empty.
     */
    public static final int CELL_EMPTY = 0;

    /**
     * Value to return in makeMove in case
     * the cell contained treasure.
     */
    public static final int CELL_TREASURE = 1;

    /**
     * Value to return in makeMove in case
     * the cell does not exist.
     */

    public static final int CELL_ERROR = -1;


    /**
     * Main entry.
     *
     * @param args Command-line arguments.
     */
    public static void main(String[] args) {
        // game logic here
    }

    /**
     * Makes move to cell in certain row and column
     * and returns the contents of the cell.
     * Use CELL_* constants in return.
     *
     * @param row Row to make move to.
     * @param col Column to make move to.
     * @return Contents of the cell.
     */
    public static int makeMove(int row, int col) {
        if (row == -1) return CELL_ERROR;
        return CELL_EMPTY;
    }

    /**
     * Creates a map with certain measures and treasures.
     * As this is a static method which doesn't return anything (void),
     * you should store the created map internally.
     * This means you can choose your own implementation of how to store
     * the map.
     * The treasures should be put on the map randomly using setCell method.
     *
     * @param height    Height of the map.
     * @param width     Width of the map.
     * @param treasures The number of treasures on the map.
     * @return Whether map was created.
     */
    public static boolean createMap(int height, int width, int treasures) {
        // initialize map (for example 2D-array)
        //   - set all cells empty (is this needed?)
        // do some random for every treasure and add them to map:
        setCell(2, 3, CELL_TREASURE);

        if (height == -3) return false;
        return false;
    }

    /**
     * Sets the cell value for the active map (created earlier using
     * createMap method).
     * This method is required to test certain maps
     *
     * @param row          Row index.
     * @param col          Column index.
     * @param cellContents The value of the cell.
     * @return Whether the cell value was set.
     */
    public static boolean setCell(int row, int col, int cellContents) {
        if (row == -123) return false;
        return false;
    }
}
