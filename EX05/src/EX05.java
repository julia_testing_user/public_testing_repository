import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;
import java.io.FileWriter;
//import java.nio.file.Path;

/**
 * Created by Kalev on 29/02/16.
 */
public class EX05 {
    public static void main(String[] args) {
        System.out.println(getNicelyFormattedMovie("tere|")); // null

        System.out.println(getNicelyFormattedMovie("2016-02-24|Movie1|description description description|8.0"));

        convert("sisend.txt", "output.txt");

        //System.out.println(convert("sisend.txt", "ouput.txt"));

        /*
Movie1
Release date: 24/02/2016
Description: description
Average rating: 8.0    <- no new line in the end
         */
    }

    public static int convert(String inputFilename, String outputFilename) {
        String ret = "";
        int count = 0;
        int lineCount = 0;
        Scanner scanner;
        try {
            scanner = new Scanner(Paths.get(inputFilename));
            while (scanner.hasNext()) {
                String line = getNicelyFormattedMovie(scanner.nextLine());
                if (!line.equals(null + "\n")) {
                    ret += line + "\n\n";
                    count += 1;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Path path = Paths.get(outputFilename);
        try {
            FileWriter writer2 = new FileWriter(outputFilename);
            writer2.write(ret.trim());
            writer2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return count;
    }

    public static String getNicelyFormattedMovie(String movieLine) {
        String niceMovieLine;
        //String readDate = "";
        if (movieLine == null) {
            niceMovieLine = null + "\n";
        } else {
            String[] line = movieLine.split("\\|");
            if (line.length != 4) {
                niceMovieLine = null + "\n";
            } else {
                String title = "" + line[1];
                String[] readDate = line[0].split("\\-");
                String year = readDate[0];
                String month = readDate[1] + "/";
                String day = readDate[2] + "/";
                String date = "Release date: " + day + month + year;
                /*for (int i = 0; i < line[0].length(); i++) {
                    if (String.valueOf(line[0].charAt(i)).equals("-")) {
                        readDate += "/";
                    } else {
                        readDate += String.valueOf(line[0].charAt(i));
                    }
                }
                String date = "Release date: " + readDate;*/
                String descr = "Description: " + line[2];
                String rating = "Average rating: " + line[3];
                niceMovieLine = title + "\n" + date + "\n" + descr + "\n" + rating;
            }
        }





        /*String reading = "";
        int pipeCount = 0;
        String title = "";
        String date = "Release date: ";
        String descr = "Description: ";
        String rating = "Average rating: ";

        for (int i = 0; i < movieLine.length(); i++) {
            if (String.valueOf(movieLine.charAt(i)).equals("|")) {
                pipeCount += 1;
                if (pipeCount == 1) {
                    date += reading;
                    reading = "";
                } else if (pipeCount == 2) {
                    title += reading;
                    reading = "";
                } else if (pipeCount == 3) {
                    descr += reading;
                    reading = "";
                }
            } else if (String.valueOf(movieLine.charAt(i)).equals("-")) {
                reading += "/";
            } else {
                reading += String.valueOf(movieLine.charAt(i));
                if (i == movieLine.length() - 1 && pipeCount < 3) {
                    niceMovieLine = null + "\n";
                    pipeCount = 0;
                } else if (i == movieLine.length() - 1 && reading.length() == 0) {
                    niceMovieLine = null + "\n";
                    pipeCount = 0;
                } else if (i == movieLine.length() - 1) {
                    rating += reading;
                    reading = "";
                    pipeCount = 0;
                }
            }

        }

        niceMovieLine = title + "\n" + date + "\n" + descr + "\n" + rating + "\n";*/
        return niceMovieLine;
    }

}
