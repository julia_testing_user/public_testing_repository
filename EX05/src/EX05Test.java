import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Ago on 2016-02-22.
 */
public class EX05Test {


    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    private static final String[] WORDS = {
            "Big", "Ben", "John", "Die", "Hard", "Bond", "James",
            "Speed", "Sun", "Car", "Stop", "the", "a",
            "Run", "Walk", "Another", "Day", "Night",
            "Small", "House", "Home", "Away", "Only"
    };

    private Random rand = new Random();


    private String generateText(int minLen, int maxLen) {
        String t = "";
        for (int i = 0; i < minLen + rand.nextInt(maxLen - minLen + 1); i++) {
            t += WORDS[rand.nextInt(WORDS.length)] + " ";
        }
        return t.trim();
    }

    private String generateTitle() {
        return generateText(1, 4);
    }

    private String generateDescription() {
        return generateText(3, 23);
    }

    private String generateInputLine() {

        String ret = "";
        // date
        ret += (1955 + rand.nextInt(60));
        ret += "-";
        ret += String.format("%02d", (1 + rand.nextInt(12)));
        ret += "-";
        ret += String.format("%02d", (1 + rand.nextInt(28)));
        ret += "|";
        // title
        ret += generateTitle() + "|";
        ret += generateDescription() + "|";
        ret += rand.nextInt(1000) / 100.0;

        return ret;
    }

    private String getString(String movieLine) {
        /*

        last line \n
        \n
        first line


         */
        String[] tokens = movieLine.split("\\|");
        if (tokens.length == 4) {
            String ret = "";
            ret += tokens[1] + "\n";
            String[] dtokens = tokens[0].split("-");
            ret += "Release date: "
                    + dtokens[2] + "/"
                    + dtokens[1] + "/"
                    + dtokens[0] + "\n";
            ret += "Description: " + tokens[2] + "\n";
            ret += "Average rating: " + tokens[3]; // + "\n"; removed newlines from formatted movie
            //ret += "\n";
            return ret;
        }
        return null;
    }

    private void generateFile(String path, int rows) {
        try {
            FileWriter f = new FileWriter(path);
            for (int i = 0; i < rows; i++) {
                String line = generateInputLine();
                f.write(line);
                if (i != rows - 1) f.write("\n");
            }
            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(timeout = 1000)
    public void testSingleLine() throws Exception {
        String line = generateInputLine();
        String expected = getString(line);
        assertEquals(expected, EX05.getNicelyFormattedMovie(line).replace("\r", ""));
    }

    @Test(timeout = 1000)
    public void testNullLine() throws Exception {
        assertNull(EX05.getNicelyFormattedMovie(null));
    }

    @Test(timeout = 1000)
    public void testLineEmptyValues() throws Exception {
        String line = "1988-12-12|||0.0";
        String expected = getString(line);
        assertEquals(expected, EX05.getNicelyFormattedMovie(line).replace("\r", ""));
    }


    private void compareFiles(String inputFilename, String outputFilename) {
        int cnt = 0;

        try {

            System.out.println(Files.readAllLines(Paths.get(outputFilename)));

            BufferedReader output = new BufferedReader(new FileReader(outputFilename));
            BufferedReader input = new BufferedReader(new FileReader(inputFilename));
            String line;
            String lastLine = "a"; // to check no-new-line in the end
            while ((line = input.readLine()) != null) {
                String formatted = getString(line);
                if (formatted != null) {
                    String[] inlines = formatted.split("\n", 4);
//                    System.out.println(Arrays.toString(inlines));
                    for (String inline : inlines) {
                        String outline = output.readLine();
                        inline = inline.trim().replace("\r", "");
//                        System.out.println(inline + "==" + outline);
                        assertEquals(inline, outline);
                    }
                    lastLine = output.readLine(); // skip one line because it should be empty
                }

            }
            System.out.println(lastLine);
            assertNull(lastLine);
            output.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void testFileRandom(int rows) {
        try {
            File inFile = testFolder.newFile("test" + rows + ".in");
            String inPath = inFile.getAbsolutePath();
            generateFile(inPath, rows);
            File outFile = testFolder.newFile("test" + rows + ".out");
            String outPath = outFile.getAbsolutePath();
            //System.out.println(inFile.length());
            int cnt = EX05.convert(inPath, outPath);
            compareFiles(inPath, outPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(timeout = 1000)
    public void testConvertCount() throws Exception {
        try {
            int rows = 90 + rand.nextInt(50);
            File inFile = testFolder.newFile("test" + rows + ".in");
            String inPath = inFile.getAbsolutePath();
            generateFile(inPath, rows);
            File outFile = testFolder.newFile("test" + rows + ".out");
            String outPath = outFile.getAbsolutePath();
            //System.out.println(inFile.length());
            int cnt = EX05.convert(inPath, outPath);
            assertEquals(rows, cnt);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(timeout = 1000)
    public void testFile10Random() throws Exception {
        testFileRandom(10);
    }

    @Test(timeout = 2000)
    public void testFile100Random() throws Exception {
        testFileRandom(100);
    }

    @Test(timeout = 4000)
    public void testFile10000Random() throws Exception {
        testFileRandom(10000);
    }

    @Test(timeout = 15000)
    public void testFile1000000Random() throws Exception {
        testFileRandom(1000000);
    }

    @Test(timeout = 1000)
    public void testMissingPipe() throws Exception {
        String inFile = "testBroken.txt";
        File outFile = testFolder.newFile("testBroken.out");
        String outPath = outFile.getAbsolutePath();
        int cnt = EX05.convert(inFile, outPath);
        compareFiles(inFile, outPath);

    }

    @Test(timeout = 1000)
    public void testTestFile() throws Exception {
        String inFile = "test.txt";
        File outFile = testFolder.newFile("test.out");
        String outPath = outFile.getAbsolutePath();
        int cnt = EX05.convert(inFile, outPath);
        compareFiles(inFile, outPath);
    }
}